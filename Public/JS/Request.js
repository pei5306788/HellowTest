

//ajax请求
 function ajaxObject(){
   var xmlHttp;
   try{
      xmlHttp = new XMLHttpRequest();
   }catch(e){
      try{
         xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
      }catch(e){
         try{
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
         }catch(e){
            
            alert("您的游览器不支持AJAX!");

            return false;

         }

      }

   }
   return  xmlHttp;
}

 function ajaxPost(url,data,fnSucceed,fnFail,fnLoading){
   var ajax = ajaxObject();
   ajax.open( "post" , url , true );
   ajax.setRequestHeader( "Content-Type" , "application/json;charset=utf-8" );
   ajax.onreadystatechange = function () {
      
       if( ajax.readyState == 4 ) {
           if( ajax.status == 200 ) {
               fnSucceed( ajax.responseText,ajax.status );
           }
           else {
               fnFail( "HTTP请求错误！错误码："+ajax.status );
           }
       }
       else {
           fnLoading();
       }
   }
   ajax.send(data);
}


/*
<script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
url_Name 接口名字
type_Str post or get
formData 数据宝  // var form = document.getElementById('upload'),
                 // formData = new FormData(form);
status 可以是静笃表 0 下载中 1 是下载成功 2 是下载失败
 */
 function ajaxPostFile(url_Name,type_Str,formData,status){

  
$.ajax({
   url:url_Name,
   type:type_Str,
   data:formData,
   processData:false,
   contentType:false,
   done: function (res) {
      status(0,res);
   },
   success:function(res){
       if(res){
         status(1,res);
       }
       console.log(res);
   },
   error:function(err){
      status(2,"网络连接失败,稍后重试" + err,);
   }
});

   

}

// var test = "222";

// export {test};