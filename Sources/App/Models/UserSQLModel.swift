//
//  UserSQLModel.swift
//  App
//
//  Created by 裴建兰 on 2019/6/13.
//  用户表

import FluentSQLite
import Vapor

final class UserSQLModel: SQLiteModel {
    /// The unique identifier for this `PJLConfigModel`.
    var id: Int?  //用户id
    /// A title describing what this `PJLConfigModel` entails.
    var useName: String // 用户的账号
    
    var passWord : String //用户密码
    
    var remark : String? //备注信息
    
    /*
     用户状态登录状态中 只允许单用户登录 ，其他的地方 涉及到接口就会退出
     */
    var useStatus: String? //用户状态 0 是未登录 1 是登录中  2是 用户主动退出
    
    var token:String? //用户令牌
    
    var power:String? //用户权限 二进制的  可以判断。。。。.  目前暂定
    
    var loginTime:String?  //登录时间 时间戳
    
    var loginCity:String?//登录城市
    
    // Creates a new `Todo`.
    init(id: Int? = nil,
         useName: String,
         passWord:String,
         remark:String?="",
         useStatus:String?="",
         token:String?="",
         power:String?="",
         loginTime:String?="",
         loginCity:String?="") {
        
        self.id = id
        self.useName = useName
        self.passWord = passWord
        self.remark = remark
        self.token = token
        self.useStatus = useStatus
        self.power = power
        self.loginTime = loginTime
        self.loginCity = loginCity
    }
}

/// Allows `UserSQLModel` to be used as a dynamic migration.
extension UserSQLModel: Migration {
    
}

/// Allows `UserSQLModel` to be encoded to and decoded from HTTP messages.
extension UserSQLModel: Content {
    
}

/// Allows `UserSQLModel` to be used as a dynamic parameter in route definitions.
extension UserSQLModel: Parameter {
    
}
