//
//  PJLConfigModel.swift
//  App
//
//  Created by 裴建兰 on 2018/10/16.
//  这个就是数据库  创建的 相关数据
import FluentSQLite
import Vapor

final class PJLConfigModel: SQLiteModel {
    /// The unique identifier for this `PJLConfigModel`.
    var id: Int?
    /// A title describing what this `PJLConfigModel` entails.
    var key: String //这个是开放的保存内容
    
    var value : String //这个是需要保存的内容
    
    var remark : String //备注信息
    
    var secretkey:String //秘钥
    
    // Creates a new `Todo`.
    init(id: Int? = nil, key: String,value:String,remark:String,secretkey:String) {
        self.id = id
        self.key = key
        self.value = value
        self.remark = remark
        self.secretkey = secretkey
    }
}

/// Allows `PJLConfigModel` to be used as a dynamic migration.
extension PJLConfigModel: Migration {
    
}

/// Allows `PJLConfigModel` to be encoded to and decoded from HTTP messages.
extension PJLConfigModel: Content {
    
}

/// Allows `PJLConfigModel` to be used as a dynamic parameter in route definitions.
extension PJLConfigModel: Parameter {
    
}
