import Vapor
/// Controlers basic CRUD operations on `PJLConfigModel`s.
final class PJLConfigModelController {
    
    func addRoutes(to router:Router){
        router.get("pjlConfigModelGet", use: self.index)
        router.post("pjlConfigModelSave", use: self.create)
        router.delete("pjlConfigModel", PJLConfigModel.parameter, use: self.delete)
    }
    
//    router.get("pjlConfigModelGet", use: pjlConfigModelController.index)
//    router.post("pjlConfigModelSave", use: pjlConfigModelController.create)
//    router.delete("pjlConfigModel", Todo.parameter, use: pjlConfigModelController.delete)
    
    /// Returns a list of all `PJLConfigModel`s.
    func index(_ req: Request) throws -> Future<[PJLConfigModel]> {
        return PJLConfigModel.query(on: req).all()
    }
    
    /// Saves a decoded `PJLConfigModel` to the database.
    func create(_ req: Request) throws -> Future<PJLConfigModel> {
        return try req.content.decode(PJLConfigModel.self).flatMap { PJLConfigModel in
            return PJLConfigModel.save(on: req)
        }
    }
    
    /// Deletes a parameterized `PJLConfigModel`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(PJLConfigModel.self).flatMap { PJLConfigModel in
            return PJLConfigModel.delete(on: req)
            }.transform(to: .ok)
    }
}

