//
//  UserController.swift
//  App
//
//  Created by 裴建兰 on 2019/6/13.
//

import Vapor

final class UserController {

//    func requestRepository(repoID:Int64,comletion:(_ Repository:Any,NSError)->Void){
//
//
//        comletion("222",NSError())
//    }
//
//    func  requestUser(userId:Int64,completion:(_ User:Any?,NSError?)->Void){
//    }
    
    
    
    
    func addRoutes(to router:Router){
        
//        let future = requestRepository(repoID: 1234).ma
        //获取所有的用户列表接口
        router.get("UserGetAllApi", use: self.index)
        //注册用户接口
        router.post("reginserUserAPI", use: self.create)
        //删除接口
        router.delete("UserDeleteAPI", UserSQLModel.parameter, use: self.delete)
        //更新接口
        router.post("UserUpDataApi", use: self.upDataApi)
        
        
        //登录接口
        router.get("login", use: self.login)
        router.post("loginApi", use: self.loginApi)
        
        //获取用户信息
        router.post("getUserInfoApi", use: self.getUserInfoApi)
        router.post("getUserInfoApi_2", use: self.getUserInfoApi_2)
        
        
    }
    
    //    router.get("pjlConfigModelGet", use: pjlConfigModelController.index)
    //    router.post("pjlConfigModelSave", use: pjlConfigModelController.create)
    //    router.delete("pjlConfigModel", Todo.parameter, use: pjlConfigModelController.delete)
    
    /// Returns a list of all `UserController`s.
    func index(_ req: Request) throws -> Future<[UserSQLModel]> {
        return UserSQLModel.query(on: req).all()
    }
    
    /// Saves a decoded `UserController` to the database.
    func create(_ req: Request) throws -> Future<UserSQLModel> {
        
       
        return try req.content.decode(UserSQLModel.self).flatMap { UserSQLModel in
            
            return UserSQLModel.save(on: req)
        }
    }
    
    /// Deletes a parameterized `UserController`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(UserSQLModel.self).flatMap { UserSQLModel in
            return UserSQLModel.delete(on: req)
            }.transform(to: .ok)
    }
    
    
    func upDataApi(_ req:Request) throws-> Future<UserSQLModel>{
        
        
        return try req.content.decode(UserSQLModel.self).flatMap({ (UserSQLModel) -> EventLoopFuture<UserSQLModel> in
            return UserSQLModel.update(on: req)
        })
        
    }
    
    
    func loginApi(_ req:Request) throws-> Future<View>{
        
        
        
        
        let getLoginUsermodel : EventLoopFuture<View> = try req.content.decode(UserSQLModel.self).flatMap { (userSQLModel) -> EventLoopFuture<View> in
            
   
          
            
            let count = UserSQLModel.query(on: req).group(._and, closure: { (and) in
                
                and.filter(\.useName,.equal,userSQLModel.useName)
                and.filter(\.passWord,.equal,userSQLModel.passWord)
            
            }).all()
            
           
            let testModel : UserSQLModel = UserSQLModel(useName: "PJL", passWord: "Name")
            
        
//            let getInt: String = "\( UserSQLMod)"
//            let dic = ["userName":count]
        
                
                return   try req.view().render("login",testModel)
            
        
        
        
        
//                getLoginUsermodel
            
          
            
          }
        
     
        
        
        
         return getLoginUsermodel
//       print( "\(getLoginUsermodel.d)")
        
    }
    
    func getUserInfoApi(_ req: Request) throws -> Future<[UserSQLModel]>{ //获取单个用户信息通过账号和密码
       

        //这个方法是把请求中的数据保存到Model 里面
       return try req.content.decode(UserSQLModel.self).flatMap { userSQLModel -> Future<[UserSQLModel]> in
            
          // 如果解析成功的话 就走个这个方法 否则在上面一层就挂掉了
            return UserSQLModel.query(on: req).group(._and, closure: { (and) in

                and.filter(\.useName,.equal,userSQLModel.useName)
                and.filter(\.passWord,.equal,userSQLModel.passWord)
                }).all()

            }
    }
    
    
    func getUserInfoApi_2(_ req: Request) throws -> Future<[UserSQLModel]>{ //测试用的
        
  
        
      return  try req.content.decode(UserSQLModel.self).flatMap { (userSQLModel) in
        
        
            UserSQLModel.query(on: req).group(._and, closure:{ and in
                
                and.filter(\.useName,.equal,userSQLModel.useName)
                and.filter(\.passWord,.equal,userSQLModel.passWord)
                
            }).all()
        }
    }
        
    
    
    
//    //登录接口
//    func loginAPI(_ req:Request) throws -> Future<View>{
//
////        let getModel :Future<UserSQLModel> =  self.getUserInfoApi(req)
//        // 获取到用户信息
//
////        let getModel: UserSQLModel? = try self.getModelTest(req)
////        if getModel == nil{
////
//              return try req.view().render("login",["userName" :"密码错误"])
////        }else
////        {
////
////              return try req.view().render("login",["userName" :getModel!.useName])
////        }
//
//    }
    
//    func getModelTest(_ req:Request) throws -> UserSQLModel?{
//
//
//        let getReult:EventLoopFuture<UserSQLModel?> = try self.getUserInfoApi(req)
//
//        return  try getReult.thenThrowing { (userSQLModel) -> UserSQLModel? in
//
//            //            if(userSQLModel ==)
//
//            return userSQLModel
////            return try req.view().render("login",["userName" :userSQLModel?.useName])
//        }.wait()
//
//
//    }
    
    /**下面进入登录页面的view*/
    func login(_ req:Request) throws ->Future<View>{

         let name = "pjlTestName"
        
        return try req.view().render("login",["userName" :name])
    }
    
    
    // Says hello
//    router.get("hello1", String.parameter) { req -> Future<View> in
//
//    return try req.view().render("hello", [
//    "name": req.parameters.next(String.self)
//    ])
//    }
}
