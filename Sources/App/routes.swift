import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "Hello, world!" example
    router.get("hello") { req in
        
//        let name = try req.parameters.next(String.parameter)
        
       
        return "Hello, world!2021_1_25"
    }
    
    router.get("PJLAppInfo") { req in
        
        return getIndexLocalFile()
    }
    
    //网站开发用的
    router.get { req in
        return try req.view().render("welcome")
    }
    
    // Says hello
    router.get("hello1", String.parameter) { req -> Future<View> in
        
        return try req.view().render("hello", [
            "name": req.parameters.next(String.self)
            ])
    }
    
    //获取图片
    router.get("myHello",String.parameter){
        req->Future<View> in
        let name = try req.parameters.next(String.self)
        
        return try req.view().render("\(name)")
    }
    
    
    
    
    
    func getIndexLocalFile() -> String{
//        print(Bundle.main.bundlePath)
        let retunSting : String = "不好意思没有了哇"
        
        return retunSting

    }

    // Example of configuring a controller
    let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
    
    let pjlConfigModelController : PJLConfigModelController = PJLConfigModelController()
    pjlConfigModelController.addRoutes(to: router)
    
    let userContoller : UserController = UserController()
    userContoller.addRoutes(to: router)
    
}
